# flagship2-sandbox

This is the public repository of the sandbox for analysts participating in Flagship 2 challenge of the CyberSec4Europe pilot.

The sandbox contains two virtual machines: *Kali* and *Hive* that you as the analyst will use to complete the challenge.


## Table of contents

1. [Setup instructions](#setup)
2. [Tools installed in Kali](#kali)
3. [Tools installed in Hive](#hive)
4. [Usage tips](#tips)
5. [Troubleshooting](#troubleshooting)


## <a name="setup"></a>1. Setup instructions

1. Install the [prerequisites for Cyber Sandbox Creator](https://gitlab.ics.muni.cz/muni-kypo-csc/cyber-sandbox-creator/-/wikis/Installation#i-want-to-run-csc-sandboxes-for-most-users).

2. Download the `Vagrantfile` from this repository:
   * If you use *Linux or Mac*, use `Vagrantfile` from the directory `unix`.
   * If you use *Windows*, use `Vagrantfile` from the directory `windows`.

3. Navigate to the folder where you downloaded the `Vagrantfile` and run the command `vagrant up` in your command-line environment (e.g., Bash or Powershell).
   * This stage can take some time, please be patient :) The first setup might take from 10 minutes up to an hour (depending on your Internet connection speed) to download the operating system boxes for Vagrant. Each subsequent setup should take less than 5 minutes. In the meantime, feel free to work at something else; the process does not require any input from you.

4. After the setup is fully finished, a VirtualBox window should appear with the Kali machine running.
   * This is the *preferred way* to use the sandbox, as it is necessary for some analytical tools. Using the GUI, login with the name `kali` and password `kali`.
   * An *alternative way* to access the individual VMs (`kali` or `hive`) is via the command line. Use the command `vagrant ssh <virtual-machine-name>`.

5. After you finish working for the first day of the Flagship challenge, run `vagrant halt` in the directory where your `Vagrantfile` is. This will power off the sandbox and enable you to continue your work later. When you return the next day, continue with `vagrant up` as in Step 3.


## <a name="kali"></a>2. Tools installed in Kali 

[Kali](https://www.kali.org/) is a Linux distribution for cybersecurity and penetration testing purposes.

The following is the list of tools present on the Kali VM and guidelines on how to run them:

#### CyberChef 

- Source: https://github.com/gchq/CyberChef.git 
- Location: the folder _'CyberChef'_ on the Desktop. 
- To run (in the browser), double-click on _CyberChef_v9.3x.0.html_. 

#### Volatility (version 3.0)  

- Source: https://github.com/volatilityfoundation/volatility3.git 
- Location: the folder _'Volatility'_ on the Desktop. 
- To run, you can use the command `$ vol.py` from anywhere (`$ vol.py -h` provides all available options).

#### Wireshark 

- Installed by default. To run the GUI, use the command `$ wireshark`. 

#### Git 

- Installed by default. To see the available options, use the command `$ git`. 

#### Cincan & Docker

Source: https://gitlab.com/CinCan/tools

The following tools are installed (and usable with _CinCan_ tool): 

- **Feature_extractor** : (cincan/feature_extractor)
- **ghidra-decompiler** :  (cincan/ghidra-decompiler)
- **ioc_strings** : (cincan/ioc_strings)
- **Iocextract** : (cincan/iocextract)
- **Manalyze** : (cincan/manalyze)
- **pdf-parser** : (cincan/pdf-parser)
- **Regripper** : (cincan/regripper) 
- **Sleuthkit** : (cincan/sleuthkit) 
- **Virustotal** : (cincan/virustotal) 
- **Yara** : (cincan/yara)
- **Clamav** : (cincan/clamav) -- might not work properly, not used in the exercise 
- **Pywhois** : (cincan/pywhois) -- might not work properly, not used in the exercise

Each tool can be run by the command `$ run cincan/<tool_name> [options]`, e.g., `$ run cincan/pywhois --help`. More detailed use for each tool is described in the [CinCan repository](https://gitlab.com/CinCan/tools). 

To list all the installed tools and their versions, run `$ sudo cincan list`. 

*Warning*: Installing _ghidra-decompiler_ shows an "undefined" version and is not provided in the _cincan list_ of installed tools. However, the tool should work properly. 


## <a name="hive"></a>3. Tools installed in TheHive

**[The Hive](http://docs.thehive-project.org/)** is a Security Incident Response Platform, which is deployed locally as one node cluster with Apache Cassandra database.

The Hive's API is primarily accessible from Kali. Launch the browser inside the Kali VM and connect to `http://10.0.0.10:80/` or `http://thehive`. Use the login `admin` and password `secret`. 


**Notes for users:** The Hive service may need some time to start correctly. Therefore, wait a few seconds before accessing the website after the first VMs launch. If needed, credentials to Apache Cassandra database are as default: user=`cassandra`, password=`cassandra`. 


## <a name="tips"></a>4. Usage tips

### Change the keyboard layout in Kali 

By default, Kali uses the US English layout. However, the xfce keyboard switcher may not work. There are two ways to solve this:
* To change the keyboard layout, use the _setxkbmap_ command in the terminal (anywhere): `$ setxkbmap -layout [lang_identifier]`, e.g. `$ setxkbmap -layout gb` for UK English.
* Or, in the keyboard settings GUI, switch the toggle "use system defaults" on and off. Then, you should be able to change the layout.

### Increase the disk size in Kali 

If the default allocated disk storage is not sufficient for you, you can resize the disk following [these instructions](https://stackoverflow.com/questions/49822594/vagrant-how-to-specify-the-disk-size/60185312#60185312) with these modifications:
* The code for checking the plugin installation can be skipped.
* The `config.disksize.size` should be `device.disksize.size` and under `config.vm.define "kali"` in the [Vagrantfile](https://gitlab.fi.muni.cz/cybersec/cs4e/flagship2-sandbox/-/blob/main/unix/Vagrantfile#L36).

## <a name="troubleshooting"></a>5. Troubleshooting

Most issues and possible solutions are documented in the [Cyber Sandbox Creator wiki](https://gitlab.ics.muni.cz/muni-kypo-csc/cyber-sandbox-creator/-/wikis/2.0/Known-Issues). If you cannot find the answer there, contact *Valdemar Švábenský* on the e-mail `svabensky@ics.muni.cz` or on the exercise platform as specified by the Flagship 2 organizers.

If the Hive service is not running after a few seconds of booting the sandbox, access its VM (`$ vagrant ssh hive`) and restart it (`$ sudo systemctl restart thehive`) -- the listening port 9000 should appear in the output of command `$ nestat -plnt`.
